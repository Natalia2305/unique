<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Lodginggroupbooking;

class LodginggroupbookingController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }
	
	public function displayCalendarAction($lodgingproviderid, $searchdate)
	{
		$fcCollection = $this->assets->collection("fullCalendar");
		$fcCollection->addJs('js/moment.min.js');
		$fcCollection->addJs('js/fullcalendar.min.js');
		$fcCollection->addCss('css/fullcalendar.min.css');
		$this->view->lodgingproviderid=$lodgingproviderid; 
		$this->view->searchdate=$searchdate;
	}
	
	public function jsonAction($lodgingproviderid)
	{
		//$this->view->disable();

		$lodgingevents = Lodgingevent::findBylodgingproviderid($lodgingproviderid);
		$this->response->resetHeaders();
		$this->response->setContentType('application/json', 'UTF-8');
		$this->response->setContent(json_encode($lodgingevents));
		return $this->response->send();
	}
	
	public function addItemAction()
	{
		$lodginggroupbookingid = $this->request->getPost('lodginggroupbookingid');
		if ($this->session->has('cart')) {
			$cart = $this->session->get('cart');
			if (isset($cart[$lodginggroupbookingid])) {
				$cart[$lodginggroupbookingid]=$cart[$lodginggroupbookingid]+1; //add one to product in cart
		
					
			}
			else {
				$cart[$lodginggroupbookingid]=1; //new product in cart
			}
		}
		else {
			$cart[$lodginggroupbookingid]=1; //new cart
		}
		$this->session->set('cart',$cart); // make the cart a session variable
		

			
	
	}
	

	public function emptyCartAction()
	{
		$this->session->remove('cart');
	} 
    /**
     * Searches for lodginggroupbooking
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Lodginggroupbooking', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $lodginggroupbooking = Lodginggroupbooking::find($parameters);
        if (count($lodginggroupbooking) == 0) {
            $this->flash->notice("The search did not find any lodginggroupbooking");

            $this->dispatcher->forward([
                "controller" => "lodginggroupbooking",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $lodginggroupbooking,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
	{
		$this->view->lodgingproviders = occasion\Lodgingprovider::find();
		$this->view->standardpackages = occasion\Standardpackage::find();

	}

    /**
     * Edits a lodginggroupbooking
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $lodginggroupbooking = Lodginggroupbooking::findFirstByid($id);
            if (!$lodginggroupbooking) {
                $this->flash->error("lodginggroupbooking was not found");

                $this->dispatcher->forward([
                    'controller' => "lodginggroupbooking",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $lodginggroupbooking->getId();

            $this->tag->setDefault("id", $lodginggroupbooking->getId());
            $this->tag->setDefault("lodgingproviderid", $lodginggroupbooking->getLodgingproviderid());
            $this->tag->setDefault("standardpackageid", $lodginggroupbooking->getStandardpackageid());
            $this->tag->setDefault("timeofcheckin", $lodginggroupbooking->getTimeofcheckin());
            $this->tag->setDefault("timeofcheckout", $lodginggroupbooking->getTimeofcheckout());
            $this->tag->setDefault("noofrooms", $lodginggroupbooking->getNoofrooms());
            $this->tag->setDefault("roomtype", $lodginggroupbooking->getRoomtype());
            $this->tag->setDefault("noofbeds", $lodginggroupbooking->getNoofbeds());
            $this->tag->setDefault("cost", $lodginggroupbooking->getCost());
            $this->tag->setDefault("deposit", $lodginggroupbooking->getDeposit());
            $this->tag->setDefault("balance", $lodginggroupbooking->getBalance());
            $this->tag->setDefault("datecheckin", $lodginggroupbooking->getDatecheckin());
            $this->tag->setDefault("datecheckout", $lodginggroupbooking->getDatecheckout());
            
        }
    }

    /**
     * Creates a new lodginggroupbooking
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "lodginggroupbooking",
                'action' => 'index'
            ]);

            return;
        }

        $lodginggroupbooking = new Lodginggroupbooking();
        $lodginggroupbooking->setlodgingproviderid($this->request->getPost("lodgingproviderid"));
        $lodginggroupbooking->setstandardpackageid($this->request->getPost("standardpackageid"));
        $lodginggroupbooking->settimeofcheckin($this->request->getPost("timeofcheckin"));
        $lodginggroupbooking->settimeofcheckout($this->request->getPost("timeofcheckout"));
        $lodginggroupbooking->setnoofrooms($this->request->getPost("noofrooms"));
        $lodginggroupbooking->setroomtype($this->request->getPost("roomtype"));
        $lodginggroupbooking->setnoofbeds($this->request->getPost("noofbeds"));
        $lodginggroupbooking->setcost($this->request->getPost("cost"));
        $lodginggroupbooking->setdeposit($this->request->getPost("deposit"));
        $lodginggroupbooking->setbalance($this->request->getPost("balance"));
        $lodginggroupbooking->setdatecheckin($this->request->getPost("datecheckin"));
        $lodginggroupbooking->setdatecheckout($this->request->getPost("datecheckout"));
        

        if (!$lodginggroupbooking->save()) {
            foreach ($lodginggroupbooking->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "lodginggroupbooking",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("lodginggroupbooking was created successfully");

        $this->dispatcher->forward([
            'controller' => "lodginggroupbooking",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a lodginggroupbooking edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "lodginggroupbooking",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $lodginggroupbooking = Lodginggroupbooking::findFirstByid($id);

        if (!$lodginggroupbooking) {
            $this->flash->error("lodginggroupbooking does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "lodginggroupbooking",
                'action' => 'index'
            ]);

            return;
        }

        $lodginggroupbooking->setlodgingproviderid($this->request->getPost("lodgingproviderid"));
        $lodginggroupbooking->setstandardpackageid($this->request->getPost("standardpackageid"));
        $lodginggroupbooking->settimeofcheckin($this->request->getPost("timeofcheckin"));
        $lodginggroupbooking->settimeofcheckout($this->request->getPost("timeofcheckout"));
        $lodginggroupbooking->setnoofrooms($this->request->getPost("noofrooms"));
        $lodginggroupbooking->setroomtype($this->request->getPost("roomtype"));
        $lodginggroupbooking->setnoofbeds($this->request->getPost("noofbeds"));
        $lodginggroupbooking->setcost($this->request->getPost("cost"));
        $lodginggroupbooking->setdeposit($this->request->getPost("deposit"));
        $lodginggroupbooking->setbalance($this->request->getPost("balance"));
        $lodginggroupbooking->setdatecheckin($this->request->getPost("datecheckin"));
        $lodginggroupbooking->setdatecheckout($this->request->getPost("datecheckout"));
        

        if (!$lodginggroupbooking->save()) {

            foreach ($lodginggroupbooking->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "lodginggroupbooking",
                'action' => 'edit',
                'params' => [$lodginggroupbooking->getId()]
            ]);

            return;
        }

        $this->flash->success("lodginggroupbooking was updated successfully");

        $this->dispatcher->forward([
            'controller' => "lodginggroupbooking",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a lodginggroupbooking
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $lodginggroupbooking = Lodginggroupbooking::findFirstByid($id);
        if (!$lodginggroupbooking) {
            $this->flash->error("lodginggroupbooking was not found");

            $this->dispatcher->forward([
                'controller' => "lodginggroupbooking",
                'action' => 'index'
            ]);

            return;
        }

        if (!$lodginggroupbooking->delete()) {

            foreach ($lodginggroupbooking->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "lodginggroupbooking",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("lodginggroupbooking was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "lodginggroupbooking",
            'action' => "index"
        ]);
    }

}
