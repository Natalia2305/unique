<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Activityrating;

class ActivityratingController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
	
    }

    /**
     * Searches for activityrating
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Activityrating', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $activityrating = Activityrating::find($parameters);
        if (count($activityrating) == 0) {
            $this->flash->notice("The search did not find any activityrating");

            $this->dispatcher->forward([
                "controller" => "activityrating",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $activityrating,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

        $this->view->activitytypeid = $activitytypeid;


        
		

    }
	

    public function showRatingsAction($id)
    {
        $this->view->activityratings = activityrating::findByactivitytypeid($id);
    }

    /**
     * Edits a activityrating
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $activityrating = Activityrating::findFirstByid($id);
            if (!$activityrating) {
                $this->flash->error("activityrating was not found");

                $this->dispatcher->forward([
                    'controller' => "activityrating",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $activityrating->getId();

            $this->tag->setDefault("id", $activityrating->getId());
            $this->tag->setDefault("rating", $activityrating->getRating());
            $this->tag->setDefault("comment", $activityrating->getComment());
            $this->tag->setDefault("createdAt", $activityrating->getCreatedat());
            $this->tag->setDefault("activitytypeid", $activityrating->getActivitytypeid());
            $this->tag->setDefault("userid", $activityrating->getUserid());
            
        }
    }

    /**
     * Creates a new activityrating
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "activityrating",
                'action' => 'index'
            ]);

            return;
        }

        $activityrating = new Activityrating();
        $activityrating->setrating($this->request->getPost("rating"));
        $activityrating->setcomment($this->request->getPost("comment"));
        $activityrating->setcreatedAt($this->request->getPost("createdAt"));
        $activityrating->setactivitytypeid($this->request->getPost("activitytypeid"));
        $activityrating->setuserid($this->request->getPost("userid"));
        

        if (!$activityrating->save()) {
            foreach ($activityrating->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "activityrating",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("activityrating was created successfully");

        $this->dispatcher->forward([
            'controller' => "activityrating",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a activityrating edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "activityrating",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $activityrating = Activityrating::findFirstByid($id);

        if (!$activityrating) {
            $this->flash->error("activityrating does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "activityrating",
                'action' => 'index'
            ]);

            return;
        }

        $activityrating->setrating($this->request->getPost("rating"));
        $activityrating->setcomment($this->request->getPost("comment"));
        $activityrating->setcreatedAt($this->request->getPost("createdAt"));
        $activityrating->setactivitytypeid($this->request->getPost("activitytypeid"));
        $activityrating->setuserid($this->request->getPost("userid"));
        

        if (!$activityrating->save()) {

            foreach ($activityrating->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "activityrating",
                'action' => 'edit',
                'params' => [$activityrating->getId()]
            ]);

            return;
        }

        $this->flash->success("activityrating was updated successfully");

        $this->dispatcher->forward([
            'controller' => "activityrating",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a activityrating
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $activityrating = Activityrating::findFirstByid($id);
        if (!$activityrating) {
            $this->flash->error("activityrating was not found");

            $this->dispatcher->forward([
                'controller' => "activityrating",
                'action' => 'index'
            ]);

            return;
        }

        if (!$activityrating->delete()) {

            foreach ($activityrating->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "activityrating",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("activityrating was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "activityrating",
            'action' => "index"
        ]);
    }

}
