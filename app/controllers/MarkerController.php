<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class MarkerController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }
	
	public function jsonAction()
	{
		//$this->view->disable();
		$markers = marker::find();
		$this->response->resetHeaders();
		$this->response->setContentType('application/json', 'UTF-8');
		$this->response->setContent(json_encode($markers));
		return $this->response->send();
	}
	
	function showMapAction()
	{

	}
    /**
     * Searches for marker
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Marker', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "MarkerID";

        $marker = Marker::find($parameters);
        if (count($marker) == 0) {
            $this->flash->notice("The search did not find any marker");

            $this->dispatcher->forward([
                "controller" => "marker",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $marker,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a marker
     *
     * @param string $MarkerID
     */
    public function editAction($MarkerID)
    {
        if (!$this->request->isPost()) {

            $marker = Marker::findFirstByMarkerID($MarkerID);
            if (!$marker) {
                $this->flash->error("marker was not found");

                $this->dispatcher->forward([
                    'controller' => "marker",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->MarkerID = $marker->getMarkerid();

            $this->tag->setDefault("MarkerID", $marker->getMarkerid());
            $this->tag->setDefault("Name", $marker->getName());
            $this->tag->setDefault("Description", $marker->getDescription());
            $this->tag->setDefault("Latitude", $marker->getLatitude());
            $this->tag->setDefault("Longitude", $marker->getLongitude());
            
        }
    }

    /**
     * Creates a new marker
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "marker",
                'action' => 'index'
            ]);

            return;
        }

        $marker = new Marker();
        $marker->setname($this->request->getPost("Name"));
        $marker->setdescription($this->request->getPost("Description"));
        $marker->setlatitude($this->request->getPost("Latitude"));
        $marker->setlongitude($this->request->getPost("Longitude"));
        

        if (!$marker->save()) {
            foreach ($marker->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "marker",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("marker was created successfully");

        $this->dispatcher->forward([
            'controller' => "marker",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a marker edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "marker",
                'action' => 'index'
            ]);

            return;
        }

        $MarkerID = $this->request->getPost("MarkerID");
        $marker = Marker::findFirstByMarkerID($MarkerID);

        if (!$marker) {
            $this->flash->error("marker does not exist " . $MarkerID);

            $this->dispatcher->forward([
                'controller' => "marker",
                'action' => 'index'
            ]);

            return;
        }

        $marker->setname($this->request->getPost("Name"));
        $marker->setdescription($this->request->getPost("Description"));
        $marker->setlatitude($this->request->getPost("Latitude"));
        $marker->setlongitude($this->request->getPost("Longitude"));
        

        if (!$marker->save()) {

            foreach ($marker->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "marker",
                'action' => 'edit',
                'params' => [$marker->getMarkerid()]
            ]);

            return;
        }

        $this->flash->success("marker was updated successfully");

        $this->dispatcher->forward([
            'controller' => "marker",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a marker
     *
     * @param string $MarkerID
     */
    public function deleteAction($MarkerID)
    {
        $marker = Marker::findFirstByMarkerID($MarkerID);
        if (!$marker) {
            $this->flash->error("marker was not found");

            $this->dispatcher->forward([
                'controller' => "marker",
                'action' => 'index'
            ]);

            return;
        }

        if (!$marker->delete()) {

            foreach ($marker->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "marker",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("marker was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "marker",
            'action' => "index"
        ]);
    }

}
