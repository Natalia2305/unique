<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Standardrating;

class StandardratingController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for standardRating
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Standardrating', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $standardRating = Standardrating::find($parameters);
        if (count($standardRating) == 0) {
            $this->flash->notice("The search did not find any standardRating");

            $this->dispatcher->forward([
                "controller" => "standardRating",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $standardRating,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction($standardpackageid)
    {
        $this->view->standardpackageid = $standardpackageid;
    }

    /**
     * Edits a standardRating
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $standardRating = Standardrating::findFirstByid($id);
            if (!$standardRating) {
                $this->flash->error("standardRating was not found");

                $this->dispatcher->forward([
                    'controller' => "standardRating",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $standardRating->getId();

            $this->tag->setDefault("id", $standardRating->getId());
            $this->tag->setDefault("rating", $standardRating->getRating());
            $this->tag->setDefault("comment", $standardRating->getComment());
            $this->tag->setDefault("createdAt", $standardRating->getCreatedat());
            $this->tag->setDefault("standardpackageid", $standardRating->getStandardpackageid());
            
        }
    }

    /**
     * Creates a new standardRating
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "standardRating",
                'action' => 'index'
            ]);

            return;
        }

        $standardRating = new Standardrating();
        $standardRating->setrating($this->request->getPost("rating"));
        $standardRating->setcomment($this->request->getPost("comment"));
        $standardRating->setcreatedAt((new DateTime())->format("Y-m-d H:i:s"));
        $standardRating->setstandardpackageid($this->request->getPost("standardpackageid"));
        

        if (!$standardRating->save()) {
            foreach ($standardRating->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardRating",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("standardRating was created successfully");

        $this->dispatcher->forward([
            'controller' => "standardPackage",
            'action' => 'displaygrid'
        ]);
    }

    /**
     * Saves a standardRating edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "standardRating",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $standardRating = Standardrating::findFirstByid($id);

        if (!$standardRating) {
            $this->flash->error("standardRating does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "standardRating",
                'action' => 'index'
            ]);

            return;
        }

        $standardRating->setrating($this->request->getPost("rating"));
        $standardRating->setcomment($this->request->getPost("comment"));
        $standardRating->setcreatedAt($this->request->getPost("createdAt"));
        $standardRating->setstandardpackageid($this->request->getPost("standardpackageid"));
        

        if (!$standardRating->save()) {

            foreach ($standardRating->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardRating",
                'action' => 'edit',
                'params' => [$standardRating->getId()]
            ]);

            return;
        }

        $this->flash->success("standardRating was updated successfully");

        $this->dispatcher->forward([
            'controller' => "standardRating",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a standardRating
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $standardRating = Standardrating::findFirstByid($id);
        if (!$standardRating) {
            $this->flash->error("standardRating was not found");

            $this->dispatcher->forward([
                'controller' => "standardRating",
                'action' => 'index'
            ]);

            return;
        }

        if (!$standardRating->delete()) {

            foreach ($standardRating->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardRating",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("standardRating was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "standardRating",
            'action' => "index"
        ]);
    }

    public function showRatingsAction($id)
    {
        $this->view->standardratings = standardrating::findByStandardpackageid($id);
    }

}
