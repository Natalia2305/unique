<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Specialrequest;

class SpecialrequestController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for specialrequest
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Specialrequest', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $specialrequest = Specialrequest::find($parameters);
        if (count($specialrequest) == 0) {
            $this->flash->notice("The search did not find any specialrequest");

            $this->dispatcher->forward([
                "controller" => "specialrequest",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $specialrequest,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    public function successAction()
    {

    }

    /**
     * Edits a specialrequest
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $specialrequest = Specialrequest::findFirstByid($id);
            if (!$specialrequest) {
                $this->flash->error("specialrequest was not found");

                $this->dispatcher->forward([
                    'controller' => "specialrequest",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $specialrequest->getId();

            $this->tag->setDefault("id", $specialrequest->getId());
            $this->tag->setDefault("administratorid", $specialrequest->getAdministratorid());
            $this->tag->setDefault("fullname", $specialrequest->getFullname());
            $this->tag->setDefault("email", $specialrequest->getEmail());
            $this->tag->setDefault("location", $specialrequest->getLocation());
            $this->tag->setDefault("budget", $specialrequest->getBudget());
            $this->tag->setDefault("preferabledate", $specialrequest->getPreferabledate());
            $this->tag->setDefault("comment", $specialrequest->getComment());
            
        }
    }

    /**
     * Creates a new specialrequest
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "specialrequest",
                'action' => 'index'
            ]);

            return;
        }

        $specialrequest = new Specialrequest();
        $specialrequest->setadministratorid($this->request->getPost("administratorid"));
        $specialrequest->setfullname($this->request->getPost("fullname"));
        $specialrequest->setemail($this->request->getPost("email", "email"));
        $specialrequest->setlocation($this->request->getPost("location"));
        $specialrequest->setbudget($this->request->getPost("budget"));
        $specialrequest->setpreferabledate($this->request->getPost("preferabledate"));
        $specialrequest->setcomment($this->request->getPost("comment"));
        $specialrequest->setSpecialrequestpic(base64_encode(file_get_contents($this->request->getUploadedFiles()[0]->getTempName())));



        if (!$specialrequest->save()) {
            foreach ($specialrequest->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "specialrequest",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("Your Request was created successfully");

        $this->dispatcher->forward([
            'controller' => "specialrequest",
            'action' => 'success'
        ]);
    }

    /**
     * Saves a specialrequest edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "specialrequest",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $specialrequest = Specialrequest::findFirstByid($id);

        if (!$specialrequest) {
            $this->flash->error("specialrequest does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "specialrequest",
                'action' => 'index'
            ]);

            return;
        }

        $specialrequest->setadministratorid($this->request->getPost("administratorid"));
        $specialrequest->setfullname($this->request->getPost("fullname"));
        $specialrequest->setemail($this->request->getPost("email", "email"));
        $specialrequest->setlocation($this->request->getPost("location"));
        $specialrequest->setbudget($this->request->getPost("budget"));
        $specialrequest->setpreferabledate($this->request->getPost("preferabledate"));
        $specialrequest->setcomment($this->request->getPost("comment"));
        

        if (!$specialrequest->save()) {

            foreach ($specialrequest->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "specialrequest",
                'action' => 'edit',
                'params' => [$specialrequest->getId()]
            ]);

            return;
        }

        $this->flash->success("specialrequest was updated successfully");

        $this->dispatcher->forward([
            'controller' => "specialrequest",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a specialrequest
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $specialrequest = Specialrequest::findFirstByid($id);
        if (!$specialrequest) {
            $this->flash->error("specialrequest was not found");

            $this->dispatcher->forward([
                'controller' => "specialrequest",
                'action' => 'index'
            ]);

            return;
        }

        if (!$specialrequest->delete()) {

            foreach ($specialrequest->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "specialrequest",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("specialrequest was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "specialrequest",
            'action' => "index"
        ]);
    }

}
