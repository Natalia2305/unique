<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Lodgingrating;

class LodgingratingController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for lodgingrating
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Lodgingrating', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $lodgingrating = Lodgingrating::find($parameters);
        if (count($lodgingrating) == 0) {
            $this->flash->notice("The search did not find any lodgingrating");

            $this->dispatcher->forward([
                "controller" => "lodgingrating",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $lodgingrating,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        $this->view->lodgingtypeid = $lodgingtypeid;

    }

    public function showRatingsAction($id)
    {
        $this->view->lodgingratings = Lodgingrating::findByLodgingtypeid($id);
    }
    /**
     * Edits a lodgingrating
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $lodgingrating = Lodgingrating::findFirstByid($id);
            if (!$lodgingrating) {
                $this->flash->error("lodgingrating was not found");

                $this->dispatcher->forward([
                    'controller' => "lodgingrating",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $lodgingrating->getId();

            $this->tag->setDefault("id", $lodgingrating->getId());
            $this->tag->setDefault("rating", $lodgingrating->getRating());
            $this->tag->setDefault("comment", $lodgingrating->getComment());
            $this->tag->setDefault("createdAt", $lodgingrating->getCreatedat());
            $this->tag->setDefault("lodgingtypeid", $lodgingrating->getLodgingtypeid());
            $this->tag->setDefault("userid", $lodgingrating->getUserid());
            
        }
    }

    /**
     * Creates a new lodgingrating
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "lodgingrating",
                'action' => 'index'
            ]);

            return;
        }

        $lodgingrating = new Lodgingrating();
        $lodgingrating->setrating($this->request->getPost("rating"));
        $lodgingrating->setcomment($this->request->getPost("comment"));
        $lodgingrating->setcreatedAt($this->request->getPost("createdAt"));
        $lodgingrating->setlodgingtypeid($this->request->getPost("lodgingtypeid"));
        $lodgingrating->setuserid($this->request->getPost("userid"));
        

        if (!$lodgingrating->save()) {
            foreach ($lodgingrating->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "lodgingrating",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("lodgingrating was created successfully");

        $this->dispatcher->forward([
            'controller' => "lodgingrating",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a lodgingrating edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "lodgingrating",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $lodgingrating = Lodgingrating::findFirstByid($id);

        if (!$lodgingrating) {
            $this->flash->error("lodgingrating does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "lodgingrating",
                'action' => 'index'
            ]);

            return;
        }

        $lodgingrating->setrating($this->request->getPost("rating"));
        $lodgingrating->setcomment($this->request->getPost("comment"));
        $lodgingrating->setcreatedAt($this->request->getPost("createdAt"));
        $lodgingrating->setlodgingtypeid($this->request->getPost("lodgingtypeid"));
        $lodgingrating->setuserid($this->request->getPost("userid"));
        

        if (!$lodgingrating->save()) {

            foreach ($lodgingrating->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "lodgingrating",
                'action' => 'edit',
                'params' => [$lodgingrating->getId()]
            ]);

            return;
        }

        $this->flash->success("lodgingrating was updated successfully");

        $this->dispatcher->forward([
            'controller' => "lodgingrating",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a lodgingrating
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $lodgingrating = Lodgingrating::findFirstByid($id);
        if (!$lodgingrating) {
            $this->flash->error("lodgingrating was not found");

            $this->dispatcher->forward([
                'controller' => "lodgingrating",
                'action' => 'index'
            ]);

            return;
        }

        if (!$lodgingrating->delete()) {

            foreach ($lodgingrating->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "lodgingrating",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("lodgingrating was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "lodgingrating",
            'action' => "index"
        ]);
    }

}
