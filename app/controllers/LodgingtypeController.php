<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Lodgingtype;

class LodgingtypeController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

	public function displayCalendarAction()
	{
		$fcCollection = $this->assets->collection("fullCalendar");
		$fcCollection->addJs('js/moment.min.js');
		$fcCollection->addJs('js/fullcalendar.min.js');
		$fcCollection->addCss('css/fullcalendar.min.css');
	}
	
	public function displayGridAction()
    {
        $this->view->lodgingtypes = lodgingtype::find();


    }
	
	public function jsonAction($lodgingproviderid)
	{
		//$this->view->disable();
		

		$events = Event::findBylodgingproviderid($lodgingproviderid);
		$this->response->resetHeaders();
		$this->response->setContentType('application/json', 'UTF-8');
		$this->response->setContent(json_encode($events));
		return $this->response->send();
	}
    /**
     * Searches for lodgingtype
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\lodgingtype', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $lodgingtype = lodgingtype::find($parameters);
        if (count($lodgingtype) == 0) {
            $this->flash->notice("The search did not find any lodgingtype");

            $this->dispatcher->forward([
                "controller" => "lodgingtype",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $lodgingtype,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
	{
		$this->view->lodgingproviders = occasion\Lodgingprovider::find();

	}

    /**
     * Edits a lodgingtype
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $lodgingtype = Lodgingtype::findFirstByid($id);
            if (!$lodgingtype) {
                $this->flash->error("lodgingtype was not found");

                $this->dispatcher->forward([
                    'controller' => "lodgingtype",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $lodgingtype->getId();

            $this->tag->setDefault("lodgingtypeid", $lodgingtype->getlodgingtypeid());
            $this->tag->setDefault("lodgingproviderid", $lodgingtype->getlodgingproviderid());
            $this->tag->setDefault("roomname", $lodgingtype->getRoomname());
            $this->tag->setDefault("id", $lodgingtype->getId());
            $this->tag->setDefault("lodgingproviderid", $lodgingtype->getLodgingproviderid());
            $this->tag->setDefault("product", $lodgingtype->getProduct());
            $this->tag->setDefault("cost", $lodgingtype->getCost());
            $this->tag->setDefault("lodgingpic", $lodgingtype->getLodgingpic());
            
        }
    }

    /**
     * Creates a new lodgingtype
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "lodgingtype",
                'action' => 'index'
            ]);

            return;
        }

        $lodgingtype = new Lodgingtype();
        $lodgingtype->setlodgingproviderid($this->request->getPost("lodgingproviderid"));
        $lodgingtype->setproduct($this->request->getPost("product"));
        $lodgingtype->setcost($this->request->getPost("cost"));
        $lodgingtype->setlodgingpic($this->request->getPost("lodgingpic"));
        

        if (!$lodgingtype->save()) {
            foreach ($lodgingtype->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "lodgingtype",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("lodgingtype was created successfully");

        $this->dispatcher->forward([
            'controller' => "lodgingtype",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a lodgingtype edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "lodgingtype",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $lodgingtype = Lodgingtype::findFirstByid($id);

        if (!$lodgingtype) {
            $this->flash->error("lodgingtype does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "lodgingtype",
                'action' => 'index'
            ]);

            return;
        }

        $lodgingtype->setlodgingproviderid($this->request->getPost("lodgingproviderid"));
        $lodgingtype->setproduct($this->request->getPost("product"));
        $lodgingtype->setcost($this->request->getPost("cost"));
        $lodgingtype->setlodgingpic($this->request->getPost("lodgingpic"));
        

        if (!$lodgingtype->save()) {

            foreach ($lodgingtype->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "lodgingtype",
                'action' => 'edit',
                'params' => [$lodgingtype->getId()]
            ]);

            return;
        }

        $this->flash->success("lodgingtype was updated successfully");

        $this->dispatcher->forward([
            'controller' => "lodgingtype",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a lodgingtype
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $lodgingtype = Lodgingtype::findFirstByid($id);
        if (!$lodgingtype) {
            $this->flash->error("lodgingtype was not found");

            $this->dispatcher->forward([
                'controller' => "lodgingtype",
                'action' => 'index'
            ]);

            return;
        }

        if (!$lodgingtype->delete()) {

            foreach ($lodgingtype->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "lodgingtype",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("lodgingtype was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "lodgingtype",
            'action' => "index"
        ]);
    }

}
