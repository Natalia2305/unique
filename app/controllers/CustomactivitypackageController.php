<?php

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Customactivitypackage;

class CustomactivitypackageController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for customactivitypackage
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Customactivitypackage', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $customactivitypackage = Customactivitypackage::find($parameters);
        if (count($customactivitypackage) == 0) {
            $this->flash->notice("The search did not find any customactivitypackage");

            $this->dispatcher->forward([
                "controller" => "customactivitypackage",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $customactivitypackage,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    public function successcreateAction()
    {

    }

    /**
     * Edits a customactivitypackage
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $customactivitypackage = Customactivitypackage::findFirstByid($id);
            if (!$customactivitypackage) {
                $this->flash->error("customactivitypackage was not found");

                $this->dispatcher->forward([
                    'controller' => "customactivitypackage",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $customactivitypackage->getId();

            $this->tag->setDefault("id", $customactivitypackage->getId());
            $this->tag->setDefault("scheduledactivityid", $customactivitypackage->getScheduledactivityid());
            $this->tag->setDefault("lodginggroupbookingid", $customactivitypackage->getLodginggroupbookingid());
            $this->tag->setDefault("groupjourneylogid", $customactivitypackage->getGroupjourneylogid());
            $this->tag->setDefault("customergroupid", $customactivitypackage->getCustomergroupid());

        }
    }

    /**
     * Creates a new customactivitypackage
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'index'
            ]);

            return;
        }

        $customactivitypackage = new Customactivitypackage();
		$customactivitypackage->setgroupjourneylogid($this->request->getPost("groupjourneylogid"));
        $customactivitypackage->setscheduledactivityid($this->request->getPost("scheduledactivityid"));
        $customactivitypackage->setlodginggroupbookingid($this->request->getPost("lodginggroupbookingid"));
<<<<<<< HEAD
   
		$customactivitypackage->setcustomergroupid($this->session->get('user')->getcustomergroup()->getId());
        
=======
        $customactivitypackage->setgroupjourneylogid($this->request->getPost("groupjourneylogid"));
        $customactivitypackage->setcustomergroupid($this->session->get('user')->getcustomergroup()->getId());

>>>>>>> f57c845a29935210765adc6ebb3fc3d47493f5e0

        if (!$customactivitypackage->save()) {
            foreach ($customactivitypackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'new'
            ]);

            return;
        }
<<<<<<< HEAD


        
=======
        $this->flash->success("customactivitypackage was created successfully");
>>>>>>> f57c845a29935210765adc6ebb3fc3d47493f5e0

        $this->dispatcher->forward([
            'controller' => "customactivitypackage",
            'action' => 'index'
        ]);
<<<<<<< HEAD


		if ($this->session->get('totalItems')==null) {
			$this->session->set('totalItems', 0);
		}
		$totalItems=$this->session->get('totalItems');
		$totalItems=$totalItems+1;
		$this->session->set('totalItems', $totalItems);

	
		 return $this->dispatcher->forward(["controller" => "customactivitypackage","action" => "successcreate"]);
=======
        if ($this->session->get('totalItems')==null) {
            $this->session->set('totalItems', 0);
        }
        $totalItems=$this->session->get('totalItems');
        $totalItems=$totalItems+1;
        $this->session->set('totalItems', $totalItems);

        $this->flash->success("Your booking was created successfully, please pay now");

        return $this->dispatcher->forward(["controller" => "customactivitypackage","action" => "successcreate"]);
>>>>>>> f57c845a29935210765adc6ebb3fc3d47493f5e0

    }

    /**
     * Saves a customactivitypackage edited
     *
     */

    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $customactivitypackage = Customactivitypackage::findFirstByid($id);

        if (!$customactivitypackage) {
            $this->flash->error("customactivitypackage does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'index'
            ]);

            return;
        }

        $customactivitypackage->setscheduledactivityid($this->request->getPost("scheduledactivityid"));
        $customactivitypackage->setlodginggroupbookingid($this->request->getPost("lodginggroupbookingid"));
        $customactivitypackage->setgroupjourneylogid($this->request->getPost("groupjourneylogid"));
        $customactivitypackage->setcustomergroupid($this->request->getPost("customergroupid"));


        if (!$customactivitypackage->save()) {

            foreach ($customactivitypackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'edit',
                'params' => [$customactivitypackage->getId()]
            ]);

            return;
        }

        $this->flash->success("customactivitypackage was updated successfully");

        $this->dispatcher->forward([
            'controller' => "customactivitypackage",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a customactivitypackage
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $customactivitypackage = Customactivitypackage::findFirstByid($id);
        if (!$customactivitypackage) {
            $this->flash->error("customactivitypackage was not found");

            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'index'
            ]);

            return;
        }

        if (!$customactivitypackage->delete()) {

            foreach ($customactivitypackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("customactivitypackage was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "customactivitypackage",
            'action' => "index"
        ]);
    }

}
