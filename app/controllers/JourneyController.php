<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Journey;

class JourneyController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

	public function displayGridAction()
	{
		$this->view->journey = Journey::find();
	} 
	
    /**
     * Searches for journey
     */

    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Journey', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $journey = Journey::find($parameters);
        if (count($journey) == 0) {
            $this->flash->notice("The search did not find any journey");

            $this->dispatcher->forward([
                "controller" => "journey",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $journey,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a journey
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $journey = Journey::findFirstByid($id);
            if (!$journey) {
                $this->flash->error("journey was not found");

                $this->dispatcher->forward([
                    'controller' => "journey",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $journey->getId();

            $this->tag->setDefault("id", $journey->getId());
            $this->tag->setDefault("administratorid", $journey->getAdministratorid());
            $this->tag->setDefault("transportproviderid", $journey->getTransportproviderid());
            $this->tag->setDefault("startpoint", $journey->getStartpoint());
            $this->tag->setDefault("destination", $journey->getDestination());
            $this->tag->setDefault("totalkilometres", $journey->getTotalkilometres());
            $this->tag->setDefault("cost", $journey->getCost());
			$this->tag->setDefault("journeypic", $journey->getJourneyPic());
            
            
        }
    }

    /**
     * Creates a new journey
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "journey",
                'action' => 'index'
            ]);

            return;
        }

        $journey = new Journey();
        $journey->setadministratorid($this->request->getPost("administratorid"));
        $journey->settransportproviderid($this->request->getPost("transportproviderid"));
        $journey->setstartpoint($this->request->getPost("startpoint"));
        $journey->setdestination($this->request->getPost("destination"));
        $journey->settotalkilometres($this->request->getPost("totalkilometres"));
        $journey->setcost($this->request->getPost("cost"));
        $journey->setJourneyPic(base64_encode(file_get_contents($this->request->getUploadedFiles()[0]->getTempName())));
        

        if (!$journey->save()) {
            foreach ($journey->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "journey",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("journey was created successfully");

        $this->dispatcher->forward([
            'controller' => "journey",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a journey edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "journey",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $journey = Journey::findFirstByid($id);

        if (!$journey) {
            $this->flash->error("journey does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "journey",
                'action' => 'index'
            ]);

            return;
        }

        $journey->setadministratorid($this->request->getPost("administratorid"));
        $journey->settransportproviderid($this->request->getPost("transportproviderid"));
        $journey->setstartpoint($this->request->getPost("startpoint"));
        $journey->setdestination($this->request->getPost("destination"));
        $journey->settotalkilometres($this->request->getPost("totalkilometres"));
        $journey->setcost($this->request->getPost("cost"));
        $journey->setjourneypic($this->request->getPost("journeypic"));
        

        if (!$journey->save()) {

            foreach ($journey->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "journey",
                'action' => 'edit',
                'params' => [$journey->getId()]
            ]);

            return;
        }

        $this->flash->success("journey was updated successfully");

        $this->dispatcher->forward([
            'controller' => "journey",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a journey
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $journey = Journey::findFirstByid($id);
        if (!$journey) {
            $this->flash->error("journey was not found");

            $this->dispatcher->forward([
                'controller' => "journey",
                'action' => 'index'
            ]);

            return;
        }

        if (!$journey->delete()) {

            foreach ($journey->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "journey",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("journey was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "journey",
            'action' => "index"
        ]);
    }

}
