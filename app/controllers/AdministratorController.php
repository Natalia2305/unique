<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Administrator;

class AdministratorController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

	public function loginAction()
    {

    }
	public function displayAction()
    {
	$numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Administrator', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $administrator = Administrator::find($parameters);
        if (count($administrator) == 0) {
            $this->flash->notice("The search did not find any learningobjective");

            $this->dispatcher->forward([
                "controller" => "administrator",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $administrator,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
 
    }
	 public function sendEmailAction($toAddress, $subject, $body)
	{
		$transport = new Swift_SmtpTransport('smtp.gmail.com',587,'tls');
		$transport->setUsername('uniqueoccasion.uo@gmail.com');
		$transport->setPassword('Unique@2019');
		$transport->setStreamOptions(array('ssl' => array('allow_self_signed' => true, 'verify_peer' => false)));
		$mailer = new Swift_Mailer($transport);
		$message = new Swift_Message($subject);
		$message->setFrom(['uniqueoccasion.uo@gmail.com' => 'Unique Occasions']);
		$message->setTo([$toAddress, $toAddress => $toAddress]);
		$message->setBody($body);
		$result = $mailer->send($message);
		if ($result>0) {
			$this->flash->notice("Email sent sucessfully");
			$this->dispatcher->forward(["controller" => "user","action" => "new"]);
		}
		else {
			$this->flash->notice("Email not sent sucessfully");
			$this->dispatcher->forward(["controller" => "user","action" => "new"]);
		}
		$this->dispatcher->forward(["controller" => "user","action" => "new"]);
	}

	
	public function testAction()
	{
	 $msg = $this->request->getPost('message');
	 $this->sendEmailAction('uniqueoccasion.uo@gmail.com','Learning Objectives', $msg);
	}

    public function logoutAction()
    {
        $this->session->destroy();
        return $this->dispatcher->forward(["controller" => "administrator","action" => "login"]);
    }

    public function authorizeAction()
    {
        $email = $this->request->getPost('email');
        $pass= $this->request->getPost('password');
        $administrator=administrator::findFirstByEmail($email);
        if ($administrator) {
            if ($this->security->checkHash($pass, $administrator->getpassword())) {
                $this->session->set('auth',
                    ['email' => $administrator->getemail(),
                        'role' => $administrator->getRole()]);
                $this->flash->success("Welcome back " . $administrator->getemail());
                return $this->dispatcher->forward(["controller" => "administrator","action" => "index"]);
            }
            else {
                $this->flash->error("Your password is incorrect - try again");
                return $this->dispatcher->forward(["controller" => "administrator","action" => "login"]);
            }
        }
        else {
            $this->flash->error("That email was not found - try again");
            return $this->dispatcher->forward(["controller" => "administrator","action" => "login"]);
        }
        return $this->dispatcher->forward(["controller" => "custombooking","action" => "search"]);
    }
    /**
     * Searches for administrator
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Administrator', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $administrator = Administrator::find($parameters);
        if (count($administrator) == 0) {
            $this->flash->notice("The search did not find any administrator");

            $this->dispatcher->forward([
                "controller" => "administrator",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $administrator,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a administrator
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $administrator = Administrator::findFirstByid($id);
            if (!$administrator) {
                $this->flash->error("administrator was not found");

                $this->dispatcher->forward([
                    'controller' => "administrator",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $administrator->getId();

            $this->tag->setDefault("id", $administrator->getId());
            $this->tag->setDefault("administratorid", $administrator->getAdministratorid());
            $this->tag->setDefault("firstname", $administrator->getFirstname());
            $this->tag->setDefault("surname", $administrator->getSurname());
            $this->tag->setDefault("dob", $administrator->getDob());
            $this->tag->setDefault("mobileno", $administrator->getMobileno());
            $this->tag->setDefault("addressno", $administrator->getAddressno());
            $this->tag->setDefault("streetname", $administrator->getStreetname());
            $this->tag->setDefault("eircode", $administrator->getEircode());
            $this->tag->setDefault("city", $administrator->getCity());
            $this->tag->setDefault("county", $administrator->getCounty());
            $this->tag->setDefault("email", $administrator->getEmail());
            $this->tag->setDefault("department", $administrator->getDepartment());
            $this->tag->setDefault("reportsto", $administrator->getReportsto());
            $this->tag->setDefault("salary", $administrator->getSalary());
            $this->tag->setDefault("ppsn", $administrator->getPpsn());
            $this->tag->setDefault("status", $administrator->getStatus());
            $this->tag->setDefault("role", $administrator->getRole());
            $this->tag->setDefault("validationkey", $administrator->getValidationkey());
            $this->tag->setDefault("createdat", $administrator->getCreatedat());
            $this->tag->setDefault("updatedat", $administrator->getUpdatedat());
            $this->tag->setDefault("password", $administrator->getPassword());
            
        }
    }

    /**
     * Creates a new administrator
     */
	public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "administrator",
                'action' => 'index'
            ]);

            return;
        }

        $administrator = new Administrator();
        /*$administrator->setadministratorid($this->request->getPost("administratorid"));*/
        $administrator->setfirstname($this->request->getPost("firstname"));
        $administrator->setsurname($this->request->getPost("surname"));
        /*$administrator->setdob($this->request->getPost("dob"));
        $administrator->setmobileno($this->request->getPost("mobileno"));*/
        $administrator->setaddressno($this->request->getPost("addressno"));
        /*$administrator->setstreetname($this->request->getPost("streetname"));
        $administrator->seteircode($this->request->getPost("eircode"));*/
        $administrator->setcity($this->request->getPost("city"));
        /*$administrator->setcounty($this->request->getPost("county"));*/
        $administrator->setemail($this->request->getPost("email", "email"));
        $administrator->setdepartment($this->request->getPost("department"));
        /*$administrator->setreportsto($this->request->getPost("reportsto"));
        $administrator->setsalary($this->request->getPost("salary"));
        $administrator->setppsn($this->request->getPost("ppsn"));
        $administrator->setstatus($this->request->getPost("status"));*/
        $administrator->setrole($this->request->getPost("role"));
        /*$administrator->setvalidationkey($this->request->getPost("validationkey"));
        $administrator->setcreatedat($this->request->getPost("createdat"));*/
        $administrator->setrole("Registered Administrator");
        $administrator->setstatus("Active");
        $administrator->setvalidationkey(md5($this->request->getPost("email") . uniqid()));
        $administrator->setcreatedat((new DateTime())->format("Y-m-d H:i:s"));//will set to the current date/time
        $administrator->setpassword($this->security->hash($this->request->getPost("password")));
        

        if (!$administrator->save()) {
            foreach ($administrator->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "administrator",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("administrator was created successfully");

        $this->dispatcher->forward([
            'controller' => "administrator",
            'action' => 'login'
        ]);
    }

    /**
     * Saves a administrator edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "administrator",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $administrator = Administrator::findFirstByid($id);

        if (!$administrator) {
            $this->flash->error("administrator does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "administrator",
                'action' => 'index'
            ]);

            return;
        }

        $administrator->setadministratorid($this->request->getPost("administratorid"));
        $administrator->setfirstname($this->request->getPost("firstname"));
        $administrator->setsurname($this->request->getPost("surname"));
        $administrator->setdob($this->request->getPost("dob"));
        $administrator->setmobileno($this->request->getPost("mobileno"));
        $administrator->setaddressno($this->request->getPost("addressno"));
        $administrator->setstreetname($this->request->getPost("streetname"));
        $administrator->seteircode($this->request->getPost("eircode"));
        $administrator->setcity($this->request->getPost("city"));
        $administrator->setcounty($this->request->getPost("county"));
        $administrator->setemail($this->request->getPost("email", "email"));
        $administrator->setdepartment($this->request->getPost("department"));
        $administrator->setreportsto($this->request->getPost("reportsto"));
        $administrator->setsalary($this->request->getPost("salary"));
        $administrator->setppsn($this->request->getPost("ppsn"));
        $administrator->setstatus($this->request->getPost("status"));
        $administrator->setrole($this->request->getPost("role"));
        $administrator->setvalidationkey($this->request->getPost("validationkey"));
        $administrator->setcreatedat($this->request->getPost("createdat"));
        $administrator->setupdatedat($this->request->getPost("updatedat"));
        $administrator->setpassword($this->request->getPost("password"));
        

        if (!$administrator->save()) {

            foreach ($administrator->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "administrator",
                'action' => 'edit',
                'params' => [$administrator->getId()]
            ]);

            return;
        }

        $this->flash->success("administrator was updated successfully");

        $this->dispatcher->forward([
            'controller' => "administrator",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a administrator
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $administrator = Administrator::findFirstByid($id);
        if (!$administrator) {
            $this->flash->error("administrator was not found");

            $this->dispatcher->forward([
                'controller' => "administrator",
                'action' => 'index'
            ]);

            return;
        }

        if (!$administrator->delete()) {

            foreach ($administrator->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "administrator",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("administrator was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "administrator",
            'action' => "index"
        ]);
    }
}
