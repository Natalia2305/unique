<?php

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class ActivitytypeController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }


    public function displayGridAction()
    {
        $this->view->activitytypes = activitytype::find();


    }
    /**
     * Searches for activitytype
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Activitytype', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $activitytype = Activitytype::find($parameters);
        if (count($activitytype) == 0) {
            $this->flash->notice("The search did not find any activitytype");

            $this->dispatcher->forward([
                "controller" => "activitytype",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $activitytype,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a activitytype
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $activitytype = Activitytype::findFirstByid($id);
            if (!$activitytype) {
                $this->flash->error("activitytype was not found");

                $this->dispatcher->forward([
                    'controller' => "activitytype",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $activitytype->id;

            $this->tag->setDefault("id", $activitytype->id);
            $this->tag->setDefault("activitytypeid", $activitytype->activitytypeid);
            $this->tag->setDefault("product", $activitytype->product);
            $this->tag->setDefault("activitylevel", $activitytype->activitylevel);
            $this->tag->setDefault("activitybase", $activitytype->activitybase);
            $this->tag->setDefault("cost", $activitytype->cost);
            $this->tag->setDefault("insurance", $activitytype->insurance);
            $this->tag->setDefault("description", $activitytype->description);
        }
    }

    public function placeOrderAction()
    {
        $thisOrder = new orderdetail();
        $thisOrder->save();
        $orderID = $thisOrder->getId();
        $activitytypes = $this->request->getPost("activitytype_id");
        $quantities = $this->request->getPost("quantity");
        for($i=0;$i<sizeof($activitytype);$i++) {
            $thisOrderDetail = new OrderDetail();
            $thisOrderDetail->setcustombookingid($custombookingID);
            $thisOrderDetail->setActivitytypeid($activitytype_ids[$i]);
            $thisOrderDetail->setQuantity($quantities[$i]);
            $thisOrderDetail->save();
        }
        $this->session->remove('shopping_cart');
        $this->flash->success("The Order has been placed");
        $this->dispatcher->forward(["controller" => "activitytype"]);
    }

    /**
     * Creates a new activitytype
     */
    public function createAction()
    {


        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'index'
            ]);

            return;


        }

        $activitytype = new Activitytype();
        $activitytype->activitytypeid = $this->request->getPost("activitytypeid");
        $activitytype->product = $this->request->getPost("product");
        $activitytype->activitylevel = $this->request->getPost("activitylevel");
        $activitytype->activitybase = $this->request->getPost("activitybase");
        $activitytype->cost = $this->request->getPost("cost");
        $activitytype->description = $this->request->getPost("description");
        $activitytype->setActivitypic(base64_encode(file_get_contents($this->request->getUploadedFiles()[0]->getTempName())));


        if (!$activitytype->save()) {
            foreach ($activitytype->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("activitytype was created successfully");

        $this->dispatcher->forward([
            'controller' => "activitytype",
            'action' => 'index'
        ]);

    }

    /**
     * Saves a activitytype edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $activitytype = Activitytype::findFirstByid($id);

        if (!$activitytype) {
            $this->flash->error("activitytype does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'index'
            ]);

            return;
        }

        $activitytype->activitytypeid = $this->request->getPost("activitytypeid");
        $activitytype->product = $this->request->getPost("product");
        $activitytype->activitylevel = $this->request->getPost("activitylevel");
        $activitytype->activitybase = $this->request->getPost("activitybase");
        $activitytype->cost = $this->request->getPost("cost");
        $activitytype->insurance = $this->request->getPost("insurance");
        $activitytype->description = $this->request->getPost("description");



        if (!$activitytype->save()) {

            foreach ($activitytype->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'edit',
                'params' => [$activitytype->id]
            ]);

            return;
        }

        $this->flash->success("activitytype was updated successfully");

        $this->dispatcher->forward([
            'controller' => "activitytype",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a activitytype
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $activitytype = Activitytype::findFirstByid($id);
        if (!$activitytype) {
            $this->flash->error("activitytype was not found");

            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'index'
            ]);

            return;
        }

        if (!$activitytype->delete()) {

            foreach ($activitytype->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("activitytype was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "activitytype",
            'action' => "index"
        ]);
    }


}
