<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Standardpackage;

class StandardpackageController extends ControllerBase
{


    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }
	
    public function successcreateAction()
    {
		
    }
    /**
     * display grid action
     */
	 
    public function displayGridAction()
    {
        $this->view->standardpackages = Standardpackage::find();
    }
	
    /**
     * Searches for standardpackage
     */
	 
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Standardpackage', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $standardpackage = Standardpackage::find($parameters);
        if (count($standardpackage) == 0) {
            $this->flash->notice("The search did not find any standardpackage");

            $this->dispatcher->forward([
                "controller" => "standardpackage",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $standardpackage,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a standardpackage
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $standardpackage = Standardpackage::findFirstByid($id);
            if (!$standardpackage) {
                $this->flash->error("standardpackage was not found");

                $this->dispatcher->forward([
                    'controller' => "standardpackage",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $standardpackage->getId();

            $this->tag->setDefault("id", $standardpackage->getId());
            $this->tag->setDefault("name", $standardpackage->getName());
            $this->tag->setDefault("startdate", $standardpackage->getStartdate());
            $this->tag->setDefault("enddate", $standardpackage->getEnddate());
            $this->tag->setDefault("location", $standardpackage->getLocation());
            $this->tag->setDefault("cost", $standardpackage->getCost());
            $this->tag->setDefault("rating", $standardpackage->getRating());
            $this->tag->setDefault("standardpackagepic", $standardpackage->getStandardpackagepic());


        }
    }

    /**
     * Creates a new standardpackage
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "standardpackage",
                'action' => 'index'
            ]);

            return;
        }

        $standardpackage = new Standardpackage();
        $standardpackage->setname($this->request->getPost("name"));
        $standardpackage->setstartdate($this->request->getPost("startdate"));
        $standardpackage->setenddate($this->request->getPost("enddate"));
        $standardpackage->setlocation($this->request->getPost("location"));
        $standardpackage->setcost($this->request->getPost("cost"));
        $standardpackage->setrating($this->request->getPost("rating"));
		$standardpackage->setPackagePic(base64_encode(file_get_contents($this->request->getUploadedFiles()[0]->getTempName())));


        if (!$standardpackage->save()) {
            foreach ($standardpackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardpackage",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("standardpackage was created successfully");

        $this->dispatcher->forward([
            'controller' => "standardpackage",
            'action' => 'successcreate'
        ]);
    }

    /**
     * Saves a standardpackage edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "standardpackage",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $standardpackage = Standardpackage::findFirstByid($id);

        if (!$standardpackage) {
            $this->flash->error("standardpackage does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "standardpackage",
                'action' => 'index'
            ]);

            return;
        }

        $standardpackage->setname($this->request->getPost("name"));
        $standardpackage->setstartdate($this->request->getPost("startdate"));
        $standardpackage->setenddate($this->request->getPost("enddate"));
        $standardpackage->setlocation($this->request->getPost("location"));
        $standardpackage->setcost($this->request->getPost("cost"));
        $standardpackage->setrating($this->request->getPost("rating"));
        $standardpackage->setstandardpackagepic($this->request->getPost("standardpackagepic"));
        

        if (!$standardpackage->save()) {

            foreach ($standardpackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardpackage",
                'action' => 'edit',
                'params' => [$standardpackage->getId()]
            ]);

            return;
        }

        $this->flash->success("standardpackage was updated successfully");

        $this->dispatcher->forward([
            'controller' => "standardpackage",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a standardpackage
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $standardpackage = Standardpackage::findFirstByid($id);
        if (!$standardpackage) {
            $this->flash->error("standardpackage was not found");

            $this->dispatcher->forward([
                'controller' => "standardpackage",
                'action' => 'index'
            ]);

            return;
        }

        if (!$standardpackage->delete()) {

            foreach ($standardpackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardpackage",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("standardpackage was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "standardpackage",
            'action' => "index"
        ]);
    }

}
