<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Orderdetail;

class OrderdetailController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for orderdetail
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Orderdetail', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $orderdetail = Orderdetail::find($parameters);
        if (count($orderdetail) == 0) {
            $this->flash->notice("The search did not find any orderdetail");

            $this->dispatcher->forward([
                "controller" => "orderdetail",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $orderdetail,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a orderdetail
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $orderdetail = Orderdetail::findFirstByid($id);
            if (!$orderdetail) {
                $this->flash->error("orderdetail was not found");

                $this->dispatcher->forward([
                    'controller' => "orderdetail",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $orderdetail->getId();

            $this->tag->setDefault("id", $orderdetail->getId());
            $this->tag->setDefault("product", $orderdetail->getProduct());
            $this->tag->setDefault("quantity", $orderdetail->getQuantity());
            $this->tag->setDefault("subtotal", $orderdetail->getSubtotal());
            $this->tag->setDefault("activitytypeid", $orderdetail->getActivitytypeid());
            $this->tag->setDefault("orderid", $orderdetail->getOrderid());
            
        }
    }

    /**
     * Creates a new orderdetail
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "orderdetail",
                'action' => 'index'
            ]);

            return;
        }

        $orderdetail = new Orderdetail();
        $orderdetail->setproduct($this->request->getPost("product"));
        $orderdetail->setquantity($this->request->getPost("quantity"));
        $orderdetail->setsubtotal($this->request->getPost("subtotal"));
        $orderdetail->setactivitytypeid($this->request->getPost("activitytypeid"));
        $orderdetail->setorderid($this->request->getPost("orderid"));
        

        if (!$orderdetail->save()) {
            foreach ($orderdetail->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "orderdetail",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("orderdetail was created successfully");

        $this->dispatcher->forward([
            'controller' => "orderdetail",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a orderdetail edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "orderdetail",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $orderdetail = Orderdetail::findFirstByid($id);

        if (!$orderdetail) {
            $this->flash->error("orderdetail does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "orderdetail",
                'action' => 'index'
            ]);

            return;
        }

        $orderdetail->setproduct($this->request->getPost("product"));
        $orderdetail->setquantity($this->request->getPost("quantity"));
        $orderdetail->setsubtotal($this->request->getPost("subtotal"));
        $orderdetail->setactivitytypeid($this->request->getPost("activitytypeid"));
        $orderdetail->setorderid($this->request->getPost("orderid"));
        

        if (!$orderdetail->save()) {

            foreach ($orderdetail->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "orderdetail",
                'action' => 'edit',
                'params' => [$orderdetail->getId()]
            ]);

            return;
        }

        $this->flash->success("orderdetail was updated successfully");

        $this->dispatcher->forward([
            'controller' => "orderdetail",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a orderdetail
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $orderdetail = Orderdetail::findFirstByid($id);
        if (!$orderdetail) {
            $this->flash->error("orderdetail was not found");

            $this->dispatcher->forward([
                'controller' => "orderdetail",
                'action' => 'index'
            ]);

            return;
        }

        if (!$orderdetail->delete()) {

            foreach ($orderdetail->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "orderdetail",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("orderdetail was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "orderdetail",
            'action' => "index"
        ]);
    }

}
