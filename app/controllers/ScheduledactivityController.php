<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Scheduledactivity;

class ScheduledactivityController extends ControllerBase
{
    /**
     * Index action
     */
	 /**
 * Generate an array of string dates between 2 dates
 *
 * @param string $start Start date
 * @param string $end End date
 * @param string $format Output format (Default: Y-m-d)
 *
 * @return array
 */
	public function displayCalendarAction($activitytypeid, $searchdate)
	{
		if ($this->session->has('cart')) {
			$cart = $this->session->get('cart');
			$totalQty=0;
			foreach ($cart as $scheduledactivity => $qty) {
				$totalQty = $totalQty + $qty;
			}
			$this->view->totalItems=$totalQty;
		}
		else {
			$this->view->totalItems=0;
		}
		
		$fcCollection = $this->assets->collection("fullCalendar");
		$fcCollection->addJs('js/moment.min.js');
		$fcCollection->addJs('js/fullcalendar.min.js');
		$fcCollection->addCss('css/fullcalendar.min.css');
		$this->view->activitytypeid=$activitytypeid;
		$this->view->searchdate=$searchdate;
	
		
	}
	
	public function addItemAction()
	{
		$scheduledactivityid = $this->request->getPost('scheduledactivityid');
		if ($this->session->has('cart')) {
			$cart = $this->session->get('cart');
			if (isset($cart[$scheduledactivityid])) {
				$cart[$scheduledactivityid]=$cart[$scheduledactivityid]+1; //add one to product in cart
		
					
			}
			else {
				$cart[$scheduledactivityid]=1; //new product in cart
			}
		}
		else {
			$cart[$scheduledactivityid]=1; //new cart
		}
		$this->session->set('cart',$cart); // make the cart a session variable
		

			
	
	}
	

	
	public function jsonAction($activitytypeid)
	{
		//$this->view->disable();

		$events = Event::findByactivitytypeid($activitytypeid);
		$this->response->resetHeaders();
		$this->response->setContentType('application/json', 'UTF-8');
		$this->response->setContent(json_encode($events));
		return $this->response->send();
	}
	
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

	/**public function showscheduledactivityAction($activitytypeid)
	{
		$scheduledactivity = Scheduledactivities::findByactivitytypeid($activitytypeid);
		 
		 
		$this->view->pick("scheduledactivity/search");
		
		
		
	}*/
    /**
     * Searches for scheduledactivity
     */
	 
    public function findDateAction()
	{
		$parameters["date"] = "startdate";
		$scheduledactivity = scheduledactivity::startdate($parameters);
        if (count($scheduledactivity) == 0) {
            $this->flash->notice("The search did not find any scheduledactivity");

            $this->dispatcher->forward([
                "controller" => "scheduledactivity",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $scheduledactivity,
            'limit'=> 10
        ]);

        $this->view->page = $paginator->getPaginate();
		$this->view->pick("scheduledactivity/search");
		
    }
	
	
	public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Scheduledactivity', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $scheduledactivity = Scheduledactivity::find($parameters);
        if (count($scheduledactivity) == 0) {
            $this->flash->notice("The search did not find any scheduledactivity");

            $this->dispatcher->forward([
                "controller" => "scheduledactivity",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $scheduledactivity,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }
	
	public function findscheduledactivityAction()
    {
       $scheduledactivity= Scheduledactivity::showScheduledActivityAction($activitytypeid);

        $scheduledactivity = Scheduledactivity::find($parameters);
        if (count($scheduledactivity) == 0) {
            $this->flash->notice("The search did not find any scheduledactivity");

            $this->dispatcher->forward([
                "controller" => "scheduledactivity",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $scheduledactivity,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
		$this->view->pick("scheduledactivity/search");
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a scheduledactivity
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $scheduledactivity = Scheduledactivity::findFirstByid($id);
            if (!$scheduledactivity) {
                $this->flash->error("scheduledactivity was not found");

                $this->dispatcher->forward([
                    'controller' => "scheduledactivity",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $scheduledactivity->getId();

            $this->tag->setDefault("id", $scheduledactivity->getId());
            $this->tag->setDefault("activityproviderid", $scheduledactivity->getActivityproviderid());
            $this->tag->setDefault("activitytypeid", $scheduledactivity->getActivitytypeid());
            $this->tag->setDefault("startdate", $scheduledactivity->getStartdate());
            $this->tag->setDefault("enddate", $scheduledactivity->getEnddate());
            $this->tag->setDefault("startpoint", $scheduledactivity->getStartpoint());
            $this->tag->setDefault("endpoint", $scheduledactivity->getEndpoint());
            $this->tag->setDefault("starttime", $scheduledactivity->getStarttime());
            $this->tag->setDefault("endtime", $scheduledactivity->getEndtime());
            $this->tag->setDefault("noofparticipants", $scheduledactivity->getNoofparticipants());
            
        }
    }

    /**
     * Creates a new scheduledactivity
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "scheduledactivity",
                'action' => 'index'
            ]);

            return;
        }

        $scheduledactivity = new Scheduledactivity();
        $scheduledactivity->setactivityproviderid($this->request->getPost("activityproviderid"));
        $scheduledactivity->setactivitytypeid($this->request->getPost("activitytypeid"));
        $scheduledactivity->setstartdate($this->request->getPost("startdate"));
        $scheduledactivity->setenddate($this->request->getPost("enddate"));
        $scheduledactivity->setstartpoint($this->request->getPost("startpoint"));
        $scheduledactivity->setendpoint($this->request->getPost("endpoint"));
        $scheduledactivity->setstarttime($this->request->getPost("starttime"));
        $scheduledactivity->setendtime($this->request->getPost("endtime"));
        $scheduledactivity->setnoofparticipants($this->request->getPost("noofparticipants"));
        

        if (!$scheduledactivity->save()) {
            foreach ($scheduledactivity->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "scheduledactivity",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("scheduledactivity was created successfully");

        $this->dispatcher->forward([
            'controller' => "scheduledactivity",
            'action' => 'index'
        ]);
    }

	
    /**
     * Saves a scheduledactivity edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "scheduledactivity",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $scheduledactivity = Scheduledactivity::findFirstByid($id);

        if (!$scheduledactivity) {
            $this->flash->error("scheduledactivity does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "scheduledactivity",
                'action' => 'index'
            ]);

            return;
        }

        $scheduledactivity->setactivityproviderid($this->request->getPost("activityproviderid"));
        $scheduledactivity->setactivitytypeid($this->request->getPost("activitytypeid"));
        $scheduledactivity->setstartdate($this->request->getPost("startdate"));
        $scheduledactivity->setenddate($this->request->getPost("enddate"));
        $scheduledactivity->setstartpoint($this->request->getPost("startpoint"));
        $scheduledactivity->setendpoint($this->request->getPost("endpoint"));
        $scheduledactivity->setstarttime($this->request->getPost("starttime"));
        $scheduledactivity->setendtime($this->request->getPost("endtime"));
        $scheduledactivity->setnoofparticipants($this->request->getPost("noofparticipants"));
        

        if (!$scheduledactivity->save()) {

            foreach ($scheduledactivity->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "scheduledactivity",
                'action' => 'edit',
                'params' => [$scheduledactivity->getId()]
            ]);

            return;
        }

        $this->flash->success("scheduledactivity was updated successfully");

        $this->dispatcher->forward([
            'controller' => "scheduledactivity",
            'action' => 'index'
        ]);
    }



    /**
     * Deletes a scheduledactivity
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $scheduledactivity = Scheduledactivity::findFirstByid($id);
        if (!$scheduledactivity) {
            $this->flash->error("scheduledactivity was not found");

            $this->dispatcher->forward([
                'controller' => "scheduledactivity",
                'action' => 'index'
            ]);

            return;
        }

        if (!$scheduledactivity->delete()) {

            foreach ($scheduledactivity->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "scheduledactivity",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("scheduledactivity was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "scheduledactivity",
            'action' => "index"
        ]);
    }

}