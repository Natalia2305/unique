<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Lodgingprovider;

class LodgingproviderController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for lodgingprovider
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Lodgingprovider', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $lodgingprovider = Lodgingprovider::find($parameters);
        if (count($lodgingprovider) == 0) {
            $this->flash->notice("The search did not find any lodgingprovider");

            $this->dispatcher->forward([
                "controller" => "lodgingprovider",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $lodgingprovider,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a lodgingprovider
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $lodgingprovider = Lodgingprovider::findFirstByid($id);
            if (!$lodgingprovider) {
                $this->flash->error("lodgingprovider was not found");

                $this->dispatcher->forward([
                    'controller' => "lodgingprovider",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $lodgingprovider->getId();

            $this->tag->setDefault("id", $lodgingprovider->getId());
            $this->tag->setDefault("lodgingprovidername", $lodgingprovider->getLodgingprovidername());
            $this->tag->setDefault("lodgingtype", $lodgingprovider->getLodgingtype());
            $this->tag->setDefault("phoneno", $lodgingprovider->getPhoneno());
            $this->tag->setDefault("addressno", $lodgingprovider->getAddressno());
            $this->tag->setDefault("streetname", $lodgingprovider->getStreetname());
            $this->tag->setDefault("postalcode", $lodgingprovider->getPostalcode());
            $this->tag->setDefault("city", $lodgingprovider->getCity());
            $this->tag->setDefault("county", $lodgingprovider->getCounty());
            $this->tag->setDefault("email", $lodgingprovider->getEmail());
            $this->tag->setDefault("rating", $lodgingprovider->getRating());
            
        }
    }

    /**
     * Creates a new lodgingprovider
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "lodgingprovider",
                'action' => 'search'
            ]);

            return;
        }

        $lodgingprovider = new Lodgingprovider();
        $lodgingprovider->setlodgingprovidername($this->request->getPost("lodgingprovidername"));
        $lodgingprovider->setlodgingtype($this->request->getPost("lodgingtype"));
        $lodgingprovider->setphoneno($this->request->getPost("phoneno"));
        $lodgingprovider->setaddressno($this->request->getPost("addressno"));
        $lodgingprovider->setstreetname($this->request->getPost("streetname"));
        $lodgingprovider->setpostalcode($this->request->getPost("postalcode"));
        $lodgingprovider->setcity($this->request->getPost("city"));
        $lodgingprovider->setcounty($this->request->getPost("county"));
        $lodgingprovider->setemail($this->request->getPost("email", "email"));
        $lodgingprovider->setrating($this->request->getPost("rating"));
        

        if (!$lodgingprovider->save()) {
            foreach ($lodgingprovider->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "lodgingprovider",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("lodgingprovider was created successfully");

        $this->dispatcher->forward([
            'controller' => "lodgingprovider",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a lodgingprovider edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "lodgingprovider",
                'action' => 'new'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $lodgingprovider = Lodgingprovider::findFirstByid($id);

        if (!$lodgingprovider) {
            $this->flash->error("lodgingprovider does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "lodgingprovider",
                'action' => 'index'
            ]);

            return;
        }

        $lodgingprovider->setlodgingprovidername($this->request->getPost("lodgingprovidername"));
        $lodgingprovider->setlodgingtype($this->request->getPost("lodgingtype"));
        $lodgingprovider->setphoneno($this->request->getPost("phoneno"));
        $lodgingprovider->setaddressno($this->request->getPost("addressno"));
        $lodgingprovider->setstreetname($this->request->getPost("streetname"));
        $lodgingprovider->setpostalcode($this->request->getPost("postalcode"));
        $lodgingprovider->setcity($this->request->getPost("city"));
        $lodgingprovider->setcounty($this->request->getPost("county"));
        $lodgingprovider->setemail($this->request->getPost("email", "email"));
        $lodgingprovider->setrating($this->request->getPost("rating"));
        

        if (!$lodgingprovider->save()) {

            foreach ($lodgingprovider->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "lodgingprovider",
                'action' => 'edit',
                'params' => [$lodgingprovider->getId()]
            ]);

            return;
        }

        $this->flash->success("lodgingprovider was updated successfully");

        $this->dispatcher->forward([
            'controller' => "lodgingprovider",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a lodgingprovider
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $lodgingprovider = Lodgingprovider::findFirstByid($id);
        if (!$lodgingprovider) {
            $this->flash->error("lodgingprovider was not found");

            $this->dispatcher->forward([
                'controller' => "lodgingprovider",
                'action' => 'index'
            ]);

            return;
        }

        if (!$lodgingprovider->delete()) {

            foreach ($lodgingprovider->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "lodgingprovider",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("lodgingprovider was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "lodgingprovider",
            'action' => "index"
        ]);
    }

}
