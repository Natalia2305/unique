<?= $this->getContent() ?>
<?= $this->assets->outputCss();?>
<?= $this->assets->outputCss('fullCalendar') ?>
<?= $this->assets->outputJs('fullCalendar') ?>


<div class="container">
  <div class="page-header" style="margin-top:80px;">
    <h2>Please Choose a date</h2>      
  </div> 

<style>

.fullCalendar {
    width: 100%;
    margin: 0 auto;
	height:500px !important
}
.fc-right .fc-prev-button, .fc-right .fc-next-button{
    background-color: #b1d583;
    background-image: none;
}
</style>
<div class="container">
      

</div>
<script>
$(document).ready(function() {
    $('#calendar').fullCalendar({
		aspectRatio:4,
        header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek',
				
            },
        defaultDate: '{{searchdate}}',
		navLinks: true,
        editable: true,
		slotDuration: '00:10:00',
        eventLimit: true,
		eventColor: '#30ff1e;',
		eventClick: function(event) {
			$('#start').val(event.start.format('hh:mm'));
			$('#journeyid').val(event.journeyid);
			$('#Title').val(event.title);
			$('#fullCalModal').modal('show');
		},

		events: "{{ url('groupjourneylog/json/'~journeyid) }}",
		
		
    });
});
$(function () {
		$('body').on('click', '#submitButton', function (e) {
		$(this.form).submit();
		$('#fullCalModal').modal('hide');
	});
});
calendar.option({
  aspectRatio: 2
});
</script>

<style>
body {
    padding-bottom: 200px;
	
}
</style>

<div id="calendar"></div>
<div id="fullCalModal" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-align: center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 align="center" class="modal-title-success">Transport Booking</h3>

      </div>
      <div class="modal-body">
            <div class="container-fluid">

                <form action="{{url('customactivitypackage/create')}}" method="post">

				
					<div class="form-group">
                        <label  for="journeyid">Journey ID</label>
                        <input type="text" class="form-control" id="journeyid" name="journeyid"/>
                    </div>
					
					
					<div class="form-group">
                        <label for="date">Date</label>
                        <input type="date" class="form-control" id="date" name="date"/>
                    </div>
					
					<div class="form-group">
                        <label for="time">Time</label>
                        <input type="time" class="form-control" id="time" name="time"/>

                    </div>
					
					<div class="form-group">
                        <label  for="customergroupid">Customer ID</label>
                        <input type="text" class="form-control" id="customergroupid" name="customergroupid"/>
                    </div>

                     
					

                    <div class="modal-footer">
						<button id="addItem" type="submit"  class="btn btn-success center-block addItem" value="<?php echo $groupjourneylog;?>">Add To Cart</button>
                    </div>
					

                </form>
            </div>
       </div> 
    </div>
  </div>
</div>
</script>`