<?= $this->getContent() ?>
<?= $this->assets->outputCss();?>
<?= $this->assets->outputCss('fullCalendar') ?>
<?= $this->assets->outputJs('fullCalendar') ?>


<div class="container">
  <div class="page-header" style="margin-top:80px;">
    <h2>Please Choose a date</h2>      
  </div> 


<style>

.fullCalendar {
    width: 100%;
    margin: 0 auto;
	height:500px !important
}
.fc-right .fc-prev-button, .fc-right .fc-next-button{
    background-color: #b1d583;
    background-image: none;
}
</style>
<div class="container">
      

</div>
<script>
$(document).ready(function() {
    $('#calendar').fullCalendar({
		aspectRatio:4,
        header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek',
				
            },
        defaultDate: '{{searchdate}}',
		navLinks: true,
        editable: true,
		slotDuration: '00:10:00',
        eventLimit: true,
		eventColor: '#30ff1e;',
		eventClick: function(event) {
			$('#starttime').val(event.start.format('hh:mm'));
			$('#endtime').val(event.end.format('hh:mm'));
			$('#bookingDate').val(event.start.format('YYYY-MM-DD'));
			$('#scheduledactivityid').val(event.id);

			$('#cost').val(event.cost);
			$('#activitytypeproduct').val(event.title);
			$('#fullCalModal').modal('show');
		},

		events: "{{ url('scheduledactivity/json/'~activitytypeid) }}",
		
		
    });
});
$(function () {
		$('body').on('click', '#submitButton', function (e) {
		$(this.form).submit();
		$('#fullCalModal').modal('hide');
	});
});
calendar.option({
  aspectRatio: 2
});
</script>

<style>
body {
    padding-bottom: 200px;
	
}
</style>

<div id="calendar"></div>
<div id="fullCalModal" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-align: center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 align="center" class="modal-title-success">Activity Booking</h3>

      </div>
      <div class="modal-body">
            <div class="container-fluid">

                <form action="{{url('customactivitypackage/create')}}" method="post">


               
		<div>
		<img class="img-responsive" style="margin:0 auto;" src="img/i8.jpg" alt="">
		</div>	

					<!--<div class="form-group">
                        <label for="memerid">Customer Name</label>
                        <input type="text" class="form-control" id="customerName" name="customerName"/>
                    </div>-->
				
					<div class="form-group">
                        <label  for="scheduledactivityid">Activity ID</label>
                        <input type="text" class="form-control" id="scheduledactivityid" name="scheduledactivityid"/>
                    </div>
					
					
					<div class="form-group">
                        <label for="cost">Cost</label>
                        <input type="text" class="form-control" id="cost" name="cost"/>
                    </div>
					
					<div class="form-group">
                        <label for="starttime">Start time</label>
                        <input type="text" class="form-control" id="starttime" name="starttime"/>

                    </div>
					
					<div class="form-group">
                        <label for="endtime">End time</label>
                        <input type="text" class="form-control" id="endtime" name="endtime"/>

                    </div>

                        <label for="activitytypeproduct">Activity</label>
                        <input type="text" class="form-control" id="activitytypeproduct" name="activitytypeproduct"/>
                    </div>
					

                    <div class="modal-footer">
						<button id="addItem" type="submit"  class="btn btn-success center-block addItem" value="<?php echo $scheduledactivity;?>">Add To Cart</button>
                    </div>
					

                </form>
            </div>
       </div> 
    </div>
  </div>
</div>
</script>