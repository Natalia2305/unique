<?php

class Activitytype extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $activitytypeid;

    /**
     *
     * @var string
     */
    public $activityname;

    /**
     *
     * @var string
     */
    public $activitylevel;

    /**
     *
     * @var string
     */
    public $activitybase;

    /**
     *
     * @var string
     */
    public $cost;

    /**
     *
     * @var string
     */
    public $insurance;


    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    public function setInsurance($insurance)
    {
        $this->insurance = $insurance;

        return $this;
    }

    public function setActivitylevel($activitylevel)
    {
        $this->activitylevel = $activitylevel;

        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function setActivitybase($activitybase)
    {
        $this->activitybase = $activitybase;

        return $this;
    }
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("activitytype");
        $this->hasMany('id', 'occasion\Activityrating', 'activitytypeid', ['alias' => 'Activityrating']);
        $this->hasMany('id', 'occasion\Orderdetail', 'activitytypeid', ['alias' => 'Orderdetail']);
        $this->hasMany('id', 'occasion\Scheduledactivity', 'activitytypeid', ['alias' => 'Scheduledactivity']);
    }

    public function setActivitypic($activitypic)

    {
        $this->activitypic = $activitypic;

        return $this;

    }
    public function setId($id)

    {
        $this->id = $id;

        return $this;

    }
    public function setactivitytypeid($id)

    {
        $this->activitytypeid = $activitytypeid;

        return $this;

    }

    public function setCost($cost)

    {
        $this->cost = $cost;

        return $this;

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */

    public function getAverageRating()
    {
        $id = $this->id;
        $averageRating = \occasion\Activityrating::average(['conditions' => "activitytypeid = $id",'column' => 'rating']);
        return $averageRating;
    }

    public function getSource()
    {
        return 'Activitytype';
    }


    public function getProduct()
    {
        return $this->product;
    }

    public function getActivitylevel()
    {
        return $this->activitylevel;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getActivitypic()

    {

        return $this->activitypic;

    }
    public function getId()

    {

        return $this->id;

    }
    public function getactivitytypeid()

    {

        return $this->activitytypeid;

    }

    public function getActivitybase()
    {

        return $this->activitybase;
    }

    public function getCost()
    {

        return $this->cost;
    }

    public function getInsurance()
    {

        return $this->insurance;
    }




    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Activitytype[]|Activitytype|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Activitytype|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
