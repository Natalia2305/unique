<?php

namespace occasion;

class Journey extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $administratorid;

    /**
     *
     * @var integer
     */
    protected $transportproviderid;

    /**
     *
     * @var string
     */
    protected $startpoint;

    /**
     *
     * @var string
     */
    protected $destination;

    /**
     *
     * @var integer
     */
    protected $totalkilometres;

    /**
     *
     * @var string
     */
    protected $cost;

    /**
     *
     * @var string
     */
    protected $journeypic;
	
	public function __toString()
	{
		return $this->startpoint . " to " . $this->destination;
	}

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field administratorid
     *
     * @param integer $administratorid
     * @return $this
     */
    public function setAdministratorid($administratorid)
    {
        $this->administratorid = $administratorid;

        return $this;
    }

    /**
     * Method to set the value of field transportproviderid
     *
     * @param integer $transportproviderid
     * @return $this
     */
    public function setTransportproviderid($transportproviderid)
    {
        $this->transportproviderid = $transportproviderid;

        return $this;
    }

    /**
     * Method to set the value of field startpoint
     *
     * @param string $startpoint
     * @return $this
     */
    public function setStartpoint($startpoint)
    {
        $this->startpoint = $startpoint;

        return $this;
    }

    /**
     * Method to set the value of field destination
     *
     * @param string $destination
     * @return $this
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Method to set the value of field totalkilometres
     *
     * @param integer $totalkilometres
     * @return $this
     */
    public function setTotalkilometres($totalkilometres)
    {
        $this->totalkilometres = $totalkilometres;

        return $this;
    }

    /**
     * Method to set the value of field cost
     *
     * @param string $cost
     * @return $this
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Method to set the value of field journeypic
     *
     * @param string $journeypic
     * @return $this
     */
    public function setJourneypic($journeypic)
    {
        $this->journeypic = $journeypic;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field administratorid
     *
     * @return integer
     */
    public function getAdministratorid()
    {
        return $this->administratorid;
    }

    /**
     * Returns the value of field transportproviderid
     *
     * @return integer
     */
    public function getTransportproviderid()
    {
        return $this->transportproviderid;
    }

    /**
     * Returns the value of field startpoint
     *
     * @return string
     */
    public function getStartpoint()
    {
        return $this->startpoint;
    }

    /**
     * Returns the value of field destination
     *
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Returns the value of field totalkilometres
     *
     * @return integer
     */
    public function getTotalkilometres()
    {
        return $this->totalkilometres;
    }

    /**
     * Returns the value of field cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Returns the value of field journeypic
     *
     * @return string
     */
    public function getJourneypic()
    {
        return $this->journeypic;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("journey");
        $this->hasMany('id', 'occasion\Groupjourneylog', 'journeyid', ['alias' => 'Groupjourneylog']);
        $this->belongsTo('administratorid', 'occasion\Administrator', 'id', ['alias' => 'Administrator']);
        $this->belongsTo('transportproviderid', 'occasion\Transportprovider', 'id', ['alias' => 'Transportprovider']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'journey';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Journey[]|Journey|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Journey|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
