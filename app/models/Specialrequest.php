<?php

namespace occasion;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class Specialrequest extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=8, nullable=false)
     */
    protected $id;

    /**
     *
     * @var integer
     * @Column(column="administratorid", type="integer", length=8, nullable=true)
     */
    protected $administratorid;

    /**
     *
     * @var string
     * @Column(column="fullname", type="string", length=70, nullable=true)
     */
    protected $fullname;

    /**
     *
     * @var string
     * @Column(column="email", type="string", length=70, nullable=true)
     */
    protected $email;

    /**
     *
     * @var string
     * @Column(column="location", type="string", length=40, nullable=true)
     */
    protected $location;

    /**
     *
     * @var string
     * @Column(column="budget", type="string", length=40, nullable=true)
     */
    protected $budget;

    /**
     *
     * @var string
     * @Column(column="preferabledate", type="string", nullable=true)
     */
    protected $preferabledate;

    /**
     *
     * @var string
     * @Column(column="comment", type="string", nullable=true)
     */
    protected $comment;

    /**
     *
     * @var string
     * @Column(column="specialrequestpic", type="string", nullable=true)
     */
    protected $specialrequestpic;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field administratorid
     *
     * @param integer $administratorid
     * @return $this
     */
    public function setAdministratorid($administratorid)
    {
        $this->administratorid = $administratorid;

        return $this;
    }

    /**
     * Method to set the value of field fullname
     *
     * @param string $fullname
     * @return $this
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field location
     *
     * @param string $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Method to set the value of field budget
     *
     * @param string $budget
     * @return $this
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Method to set the value of field preferabledate
     *
     * @param string $preferabledate
     * @return $this
     */
    public function setPreferabledate($preferabledate)
    {
        $this->preferabledate = $preferabledate;

        return $this;
    }

    /**
     * Method to set the value of field comment
     *
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Method to set the value of field specialrequestpic
     *
     * @param string $specialrequestpic
     * @return $this
     */
    public function setSpecialrequestpic($specialrequestpic)
    {
        $this->specialrequestpic = $specialrequestpic;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field administratorid
     *
     * @return integer
     */
    public function getAdministratorid()
    {
        return $this->administratorid;
    }

    /**
     * Returns the value of field fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Returns the value of field budget
     *
     * @return string
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Returns the value of field preferabledate
     *
     * @return string
     */
    public function getPreferabledate()
    {
        return $this->preferabledate;
    }

    /**
     * Returns the value of field comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Returns the value of field specialrequestpic
     *
     * @return string
     */
    public function getSpecialrequestpic()
    {
        return $this->specialrequestpic;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'model'   => $this,
                    'message' => 'Please enter a correct email address',
                ]
            )
        );

        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("specialrequest");
        $this->belongsTo('administratorid', 'occasion\Administrator', 'id', ['alias' => 'Administrator']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'specialrequest';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Specialrequest[]|Specialrequest|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Specialrequest|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
