<?php

namespace occasion;

class Scheduledactivity extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $activityproviderid;

    /**
     *
     * @var integer
     */
    protected $activitytypeid;

    /**
     *
     * @var string
     */
    protected $startdate;

    /**
     *
     * @var string
     */
    protected $enddate;

    /**
     *
     * @var string
     */
    protected $startpoint;

    /**
     *
     * @var string
     */
    protected $endpoint;

    /**
     *
     * @var string
     */
    protected $starttime;

    /**
     *
     * @var string
     */
    protected $endtime;

    /**
     *
     * @var integer
     */
    protected $noofparticipants;
	
	public function __toString()
	{
		return $this->startpoint . " to " . $this->endpoint;
	}


    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field activityproviderid
     *
     * @param integer $activityproviderid
     * @return $this
     */
    public function setActivityproviderid($activityproviderid)
    {
        $this->activityproviderid = $activityproviderid;

        return $this;
    }

    /**
     * Method to set the value of field activitytypeid
     *
     * @param integer $activitytypeid
     * @return $this
     */
    public function setActivitytypeid($activitytypeid)
    {
        $this->activitytypeid = $activitytypeid;

        return $this;
    }

    /**
     * Method to set the value of field startdate
     *
     * @param string $startdate
     * @return $this
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Method to set the value of field enddate
     *
     * @param string $enddate
     * @return $this
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Method to set the value of field startpoint
     *
     * @param string $startpoint
     * @return $this
     */
    public function setStartpoint($startpoint)
    {
        $this->startpoint = $startpoint;

        return $this;
    }

    /**
     * Method to set the value of field endpoint
     *
     * @param string $endpoint
     * @return $this
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * Method to set the value of field starttime
     *
     * @param string $starttime
     * @return $this
     */
    public function setStarttime($starttime)
    {
        $this->starttime = $starttime;

        return $this;
    }

    /**
     * Method to set the value of field endtime
     *
     * @param string $endtime
     * @return $this
     */
    public function setEndtime($endtime)
    {
        $this->endtime = $endtime;

        return $this;
    }

    /**
     * Method to set the value of field noofparticipants
     *
     * @param integer $noofparticipants
     * @return $this
     */
    public function setNoofparticipants($noofparticipants)
    {
        $this->noofparticipants = $noofparticipants;

        return $this;
    }
	public static function startdate($date)
	{
		$today = new \DateInterval('P1D');
		
		$firstDate = $today->sub(new \DateInterval("P" . $date . "Y"));
		return parent::find(['conditions' => 'date < :fd:', 'bind' => ['fd' => $firstDate->format('d-m-Y')]]);
	}
	public static function enddate($date)
	{
		$today = new \DateInterval('P1D');
		
		$secondDate = $today->sub(new \DateInterval("P" . $date . "Y"));
		return parent::find(['conditions' => 'date > :ed:', 'bind' => ['sd' => $secondDate->format('d-m-Y')]]);
	}
	
    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field activityproviderid
     *
     * @return integer
     */
    public function getActivityproviderid()
    {
        return $this->activityproviderid;
    }

    /**
     * Returns the value of field activitytypeid
     *
     * @return integer
     */
    public function getActivitytypeid()
    {
        return $this->activitytypeid;
    }

    /**
     * Returns the value of field startdate
     *
     * @return string
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Returns the value of field enddate
     *
     * @return string
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Returns the value of field startpoint
     *
     * @return string
     */
    public function getStartpoint()
    {
        return $this->startpoint;
    }

    /**
     * Returns the value of field endpoint
     *
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * Returns the value of field starttime
     *
     * @return string
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Returns the value of field endtime
     *
     * @return string
     */
    public function getEndtime()
    {
        return $this->endtime;
    }

    /**
     * Returns the value of field noofparticipants
     *
     * @return integer
     */
    public function getNoofparticipants()
    {
        return $this->noofparticipants;
    }

	
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("scheduledactivity");
        $this->hasMany('id', 'occasion\Customactivitypackage', 'scheduledactivity', ['alias' => 'Customactivitypackage']);
        $this->hasMany('id', 'occasion\Standardpackagelog', 'scheduleactivitylogid', ['alias' => 'Standardpackagelog']);
        $this->belongsTo('activityproviderid', 'occasion\Activityprovider', 'id', ['alias' => 'Activityprovider']);
        $this->belongsTo('activitytypeid', 'occasion\Activitytype', 'id', ['alias' => 'Activitytype']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'scheduledactivity';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Scheduledactivity[]|Scheduledactivity|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Scheduledactivity|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
	
}
