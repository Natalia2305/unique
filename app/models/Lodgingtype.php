<?php

namespace occasion;

class Lodgingtype extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var integer
     * @Column(column="lodgingproviderid", type="integer", length=8, nullable=false)
     */
    protected $lodgingproviderid;

    /**
     *
     * @var string
     * @Column(column="product", type="string", length=50, nullable=true)
     */
    protected $product;

    /**
     *
     * @var string
     * @Column(column="cost", type="string", length=20, nullable=true)
     */
    protected $cost;

    /**
     *
     * @var string
     * @Column(column="lodgingpic", type="string", nullable=true)
     */
    protected $lodgingpic;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field lodgingproviderid
     *
     * @param integer $lodgingproviderid
     * @return $this
     */
    public function setLodgingproviderid($lodgingproviderid)
    {
        $this->lodgingproviderid = $lodgingproviderid;

        return $this;
    }

    /**
     * Method to set the value of field product
     *
     * @param string $product
     * @return $this
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Method to set the value of field cost
     *
     * @param string $cost
     * @return $this
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Method to set the value of field lodgingpic
     *
     * @param string $lodgingpic
     * @return $this
     */
    public function setLodgingpic($lodgingpic)
    {
        $this->lodgingpic = $lodgingpic;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field lodgingproviderid
     *
     * @return integer
     */
    public function getLodgingproviderid()
    {
        return $this->lodgingproviderid;
    }

    /**
     * Returns the value of field product
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Returns the value of field cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Returns the value of field lodgingpic
     *
     * @return string
     */
    public function getLodgingpic()
    {
        return $this->lodgingpic;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("lodgingtype");
        $this->belongsTo('lodgingproviderid', 'occasion\Lodgingprovider', 'id', ['alias' => 'Lodgingprovider']);
        $this->hasMany('id', 'occasion\Lodgingyrating', 'lodgingtypeid', ['alias' => 'Lodgingrating']);

    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Lodgingtype[]|Lodgingtype|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Lodgingtype|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'lodgingtype';
    }

    public function getAverageRating()
    {
        $id = $this->id;
        $averageRating = lodgingRating::average(['conditions' => "lodgingtypeid = $id",'column' => 'rating']);
        return $averageRating;
    }
}
