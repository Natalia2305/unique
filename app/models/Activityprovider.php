<?php

namespace occasion;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class Activityprovider extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $companyname;

    /**
     *
     * @var string
     */
    protected $activityname;

    /**
     *
     * @var string
     */
    protected $activitytype;

    /**
     *
     * @var string
     */
    protected $phoneno;

    /**
     *
     * @var integer
     */
    protected $addressno;

    /**
     *
     * @var string
     */
    protected $streetname;

    /**
     *
     * @var string
     */
    protected $postalcode;

    /**
     *
     * @var string
     */
    protected $city;

    /**
     *
     * @var string
     */
    protected $county;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $rating;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field companyname
     *
     * @param string $companyname
     * @return $this
     */
    public function setCompanyname($companyname)
    {
        $this->companyname = $companyname;

        return $this;
    }

    /**
     * Method to set the value of field activityname
     *
     * @param string $activityname
     * @return $this
     */
    public function setActivityname($activityname)
    {
        $this->activityname = $activityname;

        return $this;
    }

    /**
     * Method to set the value of field activitytype
     *
     * @param string $activitytype
     * @return $this
     */
    public function setActivitytype($activitytype)
    {
        $this->activitytype = $activitytype;

        return $this;
    }

    /**
     * Method to set the value of field phoneno
     *
     * @param string $phoneno
     * @return $this
     */
    public function setPhoneno($phoneno)
    {
        $this->phoneno = $phoneno;

        return $this;
    }

    /**
     * Method to set the value of field addressno
     *
     * @param integer $addressno
     * @return $this
     */
    public function setAddressno($addressno)
    {
        $this->addressno = $addressno;

        return $this;
    }

    /**
     * Method to set the value of field streetname
     *
     * @param string $streetname
     * @return $this
     */
    public function setStreetname($streetname)
    {
        $this->streetname = $streetname;

        return $this;
    }

    /**
     * Method to set the value of field postalcode
     *
     * @param string $postalcode
     * @return $this
     */
    public function setPostalcode($postalcode)
    {
        $this->postalcode = $postalcode;

        return $this;
    }

    /**
     * Method to set the value of field city
     *
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Method to set the value of field county
     *
     * @param string $county
     * @return $this
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field rating
     *
     * @param string $rating
     * @return $this
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field companyname
     *
     * @return string
     */
    public function getCompanyname()
    {
        return $this->companyname;
    }

    /**
     * Returns the value of field activityname
     *
     * @return string
     */
    public function getActivityname()
    {
        return $this->activityname;
    }

    /**
     * Returns the value of field activitytype
     *
     * @return string
     */
    public function getActivitytype()
    {
        return $this->activitytype;
    }

    /**
     * Returns the value of field phoneno
     *
     * @return string
     */
    public function getPhoneno()
    {
        return $this->phoneno;
    }

    /**
     * Returns the value of field addressno
     *
     * @return integer
     */
    public function getAddressno()
    {
        return $this->addressno;
    }

    /**
     * Returns the value of field streetname
     *
     * @return string
     */
    public function getStreetname()
    {
        return $this->streetname;
    }

    /**
     * Returns the value of field postalcode
     *
     * @return string
     */
    public function getPostalcode()
    {
        return $this->postalcode;
    }

    /**
     * Returns the value of field city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Returns the value of field county
     *
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field rating
     *
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'model'   => $this,
                    'message' => 'Please enter a correct email address',
                ]
            )
        );

        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("activityprovider");
        $this->hasMany('id', 'occasion\Scheduledactivity', 'activityproviderid', ['alias' => 'Scheduledactivity']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'activityprovider';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Activityprovider[]|Activityprovider|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Activityprovider|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
