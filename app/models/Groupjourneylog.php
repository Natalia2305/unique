<?php

namespace occasion;

class Groupjourneylog extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $date;

    /**
     *
     * @var string
     */
    protected $time;

    /**
     *
     * @var integer
     */
    protected $customergroupid;

    /**
     *
     * @var integer
     */
    protected $journeyid;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field date
     *
     * @param string $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Method to set the value of field time
     *
     * @param string $time
     * @return $this
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Method to set the value of field customergroupid
     *
     * @param integer $customergroupid
     * @return $this
     */
    public function setCustomergroupid($customergroupid)
    {
        $this->customergroupid = $customergroupid;

        return $this;
    }

    /**
     * Method to set the value of field journeyid
     *
     * @param integer $journeyid
     * @return $this
     */
    public function setJourneyid($journeyid)
    {
        $this->journeyid = $journeyid;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Returns the value of field time
     *
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Returns the value of field customergroupid
     *
     * @return integer
     */
    public function getCustomergroupid()
    {
        return $this->customergroupid;
    }

    /**
     * Returns the value of field journeyid
     *
     * @return integer
     */
    public function getJourneyid()
    {
        return $this->journeyid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("groupjourneylog");
        $this->hasMany('id', 'occasion\Customactivitypackage', 'groupjourneylogid', ['alias' => 'Customactivitypackage']);
        $this->belongsTo('customergroupid', 'occasion\Customergroup', 'id', ['alias' => 'Customergroup']);
        $this->belongsTo('journeyid', 'occasion\Journey', 'id', ['alias' => 'Journey']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'groupjourneylog';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Groupjourneylog[]|Groupjourneylog|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Groupjourneylog|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
