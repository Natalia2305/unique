<?php

namespace occasion;

class Customactivitypackage extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $scheduledactivityid;

    /**
     *
     * @var integer
     */
    protected $lodginggroupbookingid;

    /**
     *
     * @var integer
     */
    protected $groupjourneylogid;

    /**
     *
     * @var integer
     */
    protected $customergroupid;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field scheduledactivityid
     *
     * @param integer $scheduledactivityid
     * @return $this
     */
    public function setScheduledactivityid($scheduledactivityid)
    {
        $this->scheduledactivityid = $scheduledactivityid;

        return $this;
    }

    /**
     * Method to set the value of field lodginggroupbookingid
     *
     * @param integer $lodginggroupbookingid
     * @return $this
     */
    public function setLodginggroupbookingid($lodginggroupbookingid)
    {
        $this->lodginggroupbookingid = $lodginggroupbookingid;

        return $this;
    }

    /**
     * Method to set the value of field groupjourneylogid
     *
     * @param integer $groupjourneylogid
     * @return $this
     */
    public function setGroupjourneylogid($groupjourneylogid)
    {
        $this->groupjourneylogid = $groupjourneylogid;

        return $this;
    }

    /**
     * Method to set the value of field customergroupid
     *
     * @param integer $customergroupid
     * @return $this
     */
    public function setCustomergroupid($customergroupid)
    {
        $this->customergroupid = $customergroupid;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field scheduledactivityid
     *
     * @return integer
     */
    public function getScheduledactivityid()
    {
        return $this->scheduledactivityid;
    }

    /**
     * Returns the value of field lodginggroupbookingid
     *
     * @return integer
     */
    public function getLodginggroupbookingid()
    {
        return $this->lodginggroupbookingid;
    }

    /**
     * Returns the value of field groupjourneylogid
     *
     * @return integer
     */
    public function getGroupjourneylogid()
    {
        return $this->groupjourneylogid;
    }

    /**
     * Returns the value of field customergroupid
     *
     * @return integer
     */
    public function getCustomergroupid()
    {
        return $this->customergroupid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("customactivitypackage");
        $this->belongsTo('customergroupid', 'occasion\Customergroup', 'id', ['alias' => 'Customergroup']);
        $this->belongsTo('scheduledactivityid', 'occasion\Scheduledactivity', 'id', ['alias' => 'Scheduledactivity']);
        $this->belongsTo('lodginggroupbookingid', 'occasion\Lodginggroupbooking', 'id', ['alias' => 'Lodginggroupbooking']);
        $this->belongsTo('groupjourneylogid', 'occasion\Groupjourneylog', 'id', ['alias' => 'Groupjourneylog']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customactivitypackage';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customactivitypackage[]|Customactivitypackage|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customactivitypackage|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
