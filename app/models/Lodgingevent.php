<?php

class Lodgingevent extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Column(column="lodgingproviderid", type="integer", length=8, nullable=true)
     */
    protected $lodgingproviderid;

    /**
     *
     * @var string
     * @Column(column="title", type="string", nullable=true)
     */
    protected $title;

    /**
     *
     * @var string
     * @Column(column="start", type="string", length=21, nullable=true)
     */
    protected $start;

    /**
     *
     * @var string
     * @Column(column="end", type="string", length=21, nullable=true)
     */
    protected $end;

    /**
     *
     * @var string
     * @Column(column="lodginggroupbooking", type="string", length=25, nullable=false)
     */
    protected $lodginggroupbooking;

    /**
     *
     * @var integer
     * @Column(column="id", type="integer", length=8, nullable=false)
     */
    protected $id;

    /**
     * Method to set the value of field lodgingproviderid
     *
     * @param integer $lodgingproviderid
     * @return $this
     */
    public function setLodgingproviderid($lodgingproviderid)
    {
        $this->lodgingproviderid = $lodgingproviderid;

        return $this;
    }

    /**
     * Method to set the value of field title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Method to set the value of field start
     *
     * @param string $start
     * @return $this
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Method to set the value of field end
     *
     * @param string $end
     * @return $this
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Method to set the value of field lodginggroupbooking
     *
     * @param string $lodginggroupbooking
     * @return $this
     */
    public function setLodginggroupbooking($lodginggroupbooking)
    {
        $this->lodginggroupbooking = $lodginggroupbooking;

        return $this;
    }

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Returns the value of field lodgingproviderid
     *
     * @return integer
     */
    public function getLodgingproviderid()
    {
        return $this->lodgingproviderid;
    }

    /**
     * Returns the value of field title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Returns the value of field start
     *
     * @return string
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Returns the value of field end
     *
     * @return string
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Returns the value of field lodginggroupbooking
     *
     * @return string
     */
    public function getLodginggroupbooking()
    {
        return $this->lodginggroupbooking;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("lodgingevent");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'lodgingevent';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Lodgingevent[]|Lodgingevent|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Lodgingevent|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
