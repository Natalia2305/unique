<?php

class Standardpackageevent extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Column(column="standardpackageid", type="integer", length=9, nullable=true)

     */
    protected $standardpackageid;

    /**
     *
     * @var string

     * @Column(column="title", type="string", length=40, nullable=true)

     */
    protected $title;

    /**
     *
     * @var string


     * @Column(column="start", type="string", length=21, nullable=true)
     */
    protected $start;

    /**
     *
     * @var string
     * @Column(column="end", type="string", length=21, nullable=true)
     */
    protected $end;

    /**
     *
     * @var string
	 
     * @Column(column="standardpackagelog", type="string", length=26, nullable=false)

     */
    protected $standardpackagelog;

    /**
     *
     * @var integer

     * @Column(column="id", type="integer", length=8, nullable=false)

     */
    protected $id;

    /**
     * Method to set the value of field standardpackageid
     *
     * @param integer $standardpackageid
     * @return $this
     */
    public function setStandardpackageid($standardpackageid)
    {
        $this->standardpackageid = $standardpackageid;

        return $this;
    }

    /**
     * Method to set the value of field title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Method to set the value of field start
     *
     * @param string $start
     * @return $this
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Method to set the value of field end
     *
     * @param string $end
     * @return $this
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Method to set the value of field standardpackagelog
     *
     * @param string $standardpackagelog
     * @return $this
     */
    public function setStandardpackagelog($standardpackagelog)
    {
        $this->standardpackagelog = $standardpackagelog;

        return $this;
    }

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Returns the value of field standardpackageid
     *
     * @return integer
     */
    public function getStandardpackageid()
    {
        return $this->standardpackageid;
    }

    /**
     * Returns the value of field title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Returns the value of field start
     *
     * @return string
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Returns the value of field end
     *
     * @return string
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Returns the value of field standardpackagelog
     *
     * @return string
     */
    public function getStandardpackagelog()
    {
        return $this->standardpackagelog;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("standardpackageevent");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'standardpackageevent';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Standardpackageevent[]|Standardpackageevent|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Standardpackageevent|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}